@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h2 class="card-title">
                {{$post->title}}
            </h2>
                <p class="card-subtitle text-muted">
                    Author: {{$post->user->name}}
                </p>
                 <p class="card-subtitle text-muted">
                    Created at: {{$post->created_at}}
                </p>
                <p class="card-text">
                    Created at: {{$post->content}}
                </p>
                     

                @if(Auth::id() != $post->user_id)
                    <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                        @method('PUT')
                        @csrf
                        @if($post->likes->contains('user_id', Auth::id()))
                            <button type="submit" class="btn btn-danger">
                                Unlike
                            </button>
                       
                        @else
                             <button type="submit" class="btn btn-success">
                                Like
                            </button>
                        @endif


                    </form>


                          <button type="submit" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                Post Comment
                            </button>

                @endif


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Post a Comment</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <textarea class="form-control" id="message-text"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Post a Comment</button>
      </div>
    </div>
  </div>
</div>

                <div class="mt-3">
                    <a href="/posts" class="card-link">
                        View All Posts
                    </a>
                </div>
            
        </div>
    </div>
@endsection