@extends('layouts.app')

@section('content')
    <form method="POST" action="/posts">
        {{-- Cross-Site Request Forgery --}}
        @csrf
        <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>

        <div class="form-group">
            <label for="content">Content:</label>
            <textarea class="form-control" id="content" name="content" rows="3"></textarea>
        </div>
        <div>
            <button type="submit" class="btn btn-primary">Create Post</button>
        </div>
    </form>
@endsection