@extends('layouts.app')

@section('content')
{{--     <div class="card">
        <div class="card-body">
            <h2 class="card-title">
                {{$posts->title}}
            </h2>
                <p class="card-subtitle text-muted">
                    Author: {{$posts->user->name}}
                </p>
                 <p class="card-subtitle text-muted">
                    Created at: {{$posts->created_at}}
                </p>
                <p class="card-text">
                    Created at: {{$posts->content}}
                </p>
                <div class="mt-3">
                    <a href="/posts" class="card-link">
                        View All Posts
                    </a>
                </div>
            
        </div>
    </div> --}}
      <form method="POST" action="/posts/{{$posts->id}}">
        @method('PUT')
        @csrf
        {{-- Cross-Site Request Forgery --}}
        @csrf
        <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" class="form-control" id="title" name="title" value="{{$posts->title}}">
        </div>

        <div class="form-group">
            <label for="content">Content:</label>
            <textarea class="form-control" id="content" name="content" rows="3">{{$posts->content}}</textarea>
        </div>
        <div>
            <button type="submit" class="btn btn-primary">Update Post</button>
        </div>
    </form>
@endsection