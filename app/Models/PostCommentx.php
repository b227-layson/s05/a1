<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
/* 
    public function likes()
    {
        return $this->hasMany('App\Models\User');
    }

    public function comments()
    {
        return $this->belongsTo('App\Models\User');
    } */
}
