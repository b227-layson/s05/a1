<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// access the authenticated user via the Auth Facades
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostComment;
use App\Models\PostLike;


class PostController extends Controller
{
    //action to return a view containing a form for a blog post creation.
    public function create(){
        return view('posts.create');
    }

    /* 
        action to recieven form data and subsiquently store said data in the posts table.
    */
    public function store(Request $request){
       if(Auth::user()){
            $post = new Post;
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = (Auth::user()->id);

            $post->save();

            return redirect('/posts');
       }
       else{
            return redirect('/login');
       }
    }


    /* 
        action that will returnb a view showing all blog posts.
    */

    public function index(){

            $posts = Post::all()->where('isActive', true);
            
            return view('posts.index')->with('posts', $posts);

       
    }

    //Activity for s02
    //action that will return a view showing random blog posts
    public function welcome()
    {
        $posts = Post::where('isActive', 1)
            ->inRandomOrder()
            ->limit(3)
            ->get();
        return view('welcome')->with('posts', $posts);
    }


    /* 
        Action for showing only the posts authored by authenticated user / currently logged in user
    */
    public function myPosts(){
        if(Auth::user()){
            $posts = Auth::user()->posts;
            return view('posts.index')->with('posts', $posts);
        }
        else{
            return redirect('/login');
        }
    }


    /* 
        action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown

    */
    public function show($id){
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }




    /* 
// s03 a1
    1. Define a route that will return an "edit form" for a specific Post when a GET request is received at the /posts/{id}/edit endpoint.
*/
 /*    public function editForm()
    {
        if (Auth::user()) {
            $posts = Auth::user()->posts;
            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    } */


    public function edit($id)
    {
        if(Auth::user()){
            $posts = Post::find($id);

            return view('posts.edit')->with('posts', $posts);
        }
        else{
            return redirect('/login');
        }

    }




public function update(Request $request, $id){
    $posts = Post::find($id);

    // if authenticated user's ID is the same as the post's user_id
    if(Auth::user()->id == $posts->user_id){
        $posts->title = $request->input('title');
        $posts->content = $request->input('content');
        $posts->save();
        
    }
        return redirect('/posts');
 
}






public function archive($id){
    $posts = Post::find($id);

    // if authenticated user's ID is the same as the post's user_id
    if(Auth::user()->id == $posts->user_id){
        $posts->isActive = false;
        $posts->save();
    }
    return redirect('/myPosts');
}



    public function unArchive($id)
    {
        $posts = Post::find($id);

        // if authenticated user's ID is the same as the post's user_id
        if (Auth::user()->id == $posts->user_id) {
            $posts->isActive = true;
            $posts->save();
        }
        return redirect('/myPosts');
    }






    // action that will allow an authenticated user who is not the post author to toggle a like on the post being viewed.
    public function likes($id){
        $post = Post::find($id);

        // to check if there is a logged in user
        if(Auth::user()){
            $user_id = Auth::user()->id;
            // if authenticated user is not the post auther
            if($post->user_id != $user_id){
                // check if a post like has been made
                if($post->likes->contains("user_id", $user_id)){
                    // delete the like made by the user to unlike the post
                    PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
                }
                // create a new like record in the post_likes table with the user_id and posrt_id
                else{
                    // Create a new like record to like this post
                    // instantiate a new PostLike object from the PostLike model
                    $postLike = new PostLike;
                    // define the properties of the $postLike object
                    $postLike->post_id = $post->id;
                    $postLike->user_id = $user_id;

                    // save the postLike object in the database
                    $postLike->save();
                }
                return redirect("/posts/$id");
            }

            
            
        }
        else{
            return redirect('/login');
        }
    }




    public function comments(Request $request, $id){

        $post = Post::find($id);

        if(Auth::user()){
            $user_id = Auth::user()->id;
            if ($post->user_id != $user_id) {
                if ($post->comments->contains("user_id", $user_id)) {
                    // delete the like made by the user to unlike the post
                    PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
                }
                // create a new like record in the post_likes table with the user_id and posrt_id
                else {
                    // Create a new like record to like this post
                    // instantiate a new PostLike object from the PostLike model
                    $postComment = new PostComment;
                    // define the properties of the $postLike object
                 /*    $postComment->post_id = $post->id;
                    $postComment->user_id = $user_id; */
                    $postComment->content = $request->input('content');

                    // save the postLike object in the database
                    $postComment->save();
                }
                return redirect("/posts/$id");
            
            }
        }


    }

/* 

public function update(Request $request, $id){
    $posts = Post::find($id);

    // if authenticated user's ID is the same as the post's user_id
    if(Auth::user()->id == $posts->user_id){
        $posts->title = $request->input('title');
        $posts->content = $request->input('content');
        $posts->save();
        
    }
        return redirect('/posts');
 
}



*/


}
